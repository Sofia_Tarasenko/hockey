﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerScript : MonoBehaviour
{
    public Puck puck;
    public Pad pad;
    public static Vector2 bottomLeft;
    public static Vector2 topRight;

    // Start is called before the first frame update
    void Start()
    {
        bottomLeft = Camera.main.ScreenToWorldPoint(new Vector2(0, 0));
        topRight = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        Instantiate(puck);
        pad padVar1 = Instantiate(pad) as pad;
        pad padVar2 = Instantiate(pad) as pad;
        padVar1.Init(true);
        padVar2.Init(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
